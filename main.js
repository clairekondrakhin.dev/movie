// je défini mon objet movie avec ses paramètres
let movie = {
  name: "Kill Bill",
  releaseDate: 2003,
  directedBy: "Quentin Tarantino",
  actors: ["Uma Thurman", "Lucy Liu"],
  favoriteMovie: true,
  movieRating: 8.2,
  duration: 0,
  //Je crée une fonction permettant de mettre à jour la durée du filme avec 2 paramètres
  // a est équivalent aux heures et B et équivalent aux minutes
  setDuration: function (a, b) {
    //quand on exécute la fonction on donne la durée du film en minutes.
    //This permet ici de s'adapter au contexte de la variable donc ici dans la variable movie
    this.duration = a * 60 + b;
  },
};

// je donne des arguments à ma fonction 1h et 51min
movie.setDuration(1, 51);
//j'affiche la durée du film en minutes
console.log("la durée du film est " + movie.duration + " minutes");
